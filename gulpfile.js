'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var jade = require('gulp-jade');
var changed = require('gulp-changed');
var bulkSass = require('gulp-sass-bulk-import');
var concat = require('gulp-concat');

gulp.task('sass', function () {
  return gulp.src('./src/sass/application.sass')
    .pipe(bulkSass())
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: [
        "Android 2.3",
        "Android >= 4",
        "Chrome >= 20",
        "Firefox >= 24",
        "Explorer >= 8",
        "iOS >= 6",
        "Opera >= 12",
        "Safari >= 6"
      ],
      cascade: false,
      remove: true
    }))
    .pipe(sourcemaps.write('./css/maps'))
    .pipe(gulp.dest('./css'));
});

gulp.task('serve', [], function () {
  browserSync.init({
    server: ".",
    online: true,
    reloadDelay: 200,
    notify: false
  });

  gulp.watch('./src/sass/**/*.sass', ['sass']);
  gulp.watch('./src/jade/index.jade', ['jade']);
  gulp.watch('./src/js/scripts.js', ['concat_scripts']);

  gulp.watch("./index.html").on('change', browserSync.reload);
  gulp.watch("./css/styles.css").on('change', browserSync.reload);
});

gulp.task('jade', function () {
  return gulp.src('./src/jade/index.jade')
    .pipe(changed('./', {extension: '.html'}))
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest('./'))
});

gulp.task('concat_scripts', function () {
  return gulp.src([
    './bower_components/jquery/dist/jquery.min.js',
    './bower_components/select2/select2.min.js',
    './bower_components/pickmeup/js/jquery.pickmeup.min.js',
    './src/js/scripts.js'
  ])
    .pipe(concat('application.js'))
    .pipe(gulp.dest('./js/'));
});

gulp.task('compile_all', ['jade', 'sass', 'concat_scripts']);

gulp.task('default', ['serve']);
