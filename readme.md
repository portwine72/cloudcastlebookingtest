# Test Assignment for CloudCastle

To run this project you should have following tools installed:

- [npm](https://docs.npmjs.com/getting-started/installing-node)
- [Bower](https://bower.io/#install-bower)
- [gulp](http://gulpjs.com/)

To install all dependencies:

- run `npm install`
- run `bower install`

To run static server:

- run `gulp compile_all`
- run `gulp`
- open [http://localhost:3000](http://localhost:3000)
