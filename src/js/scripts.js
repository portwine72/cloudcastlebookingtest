'use strict';

var month = [];
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

$(function () {
    var current_date = new Date(),
        months_to_skip_old = 0,
        MONTHS_TO_SKIP = 6,
        pmu_instance,
        $pmu;

    $('.js-header-currency-control, .js-header-language-control').select2({
        minimumResultsForSearch: -1,
        containerCssClass: '-theme-dark',
        dropdownCssClass: '-theme-dark'
    });

    $('.js-booking-interface-rooms-control, .js-booking-interface-adults-control, .js-booking-interface-children-control').select2({
        minimumResultsForSearch: -1,
        containerCssClass: '-theme-light',
        dropdownCssClass: '-theme-light'
    });

    $('.js-selected-dates-start, .js-selected-dates-finish').text(current_date.getDate() + ' ' + month[current_date.getMonth()]);

    $pmu = $('.js-booking-interface-calendar').pickmeup({
        mode: 'range',
        flat: true,
        select_month: false,
        select_year: false,
        first_day: 0,
        min: current_date,
        max: (function () {
            var border_date = new Date();
            border_date.addMonths(MONTHS_TO_SKIP);
            return border_date;
        })(),
        render: function (date) {
            return {
                class_name: 'booking-interface-calendar__item calendar-cell'
            };
        },
        fill: function () {
            $('.pmu-days .pmu-button:not(.pmu-disabled)').each(function() {
                $(this).append('<span class="calendar-cell__price">$250</span>');
            });

            return true;
        },
        change: function(date, dates_obj) {
            $('.js-selected-dates-start').text(dates_obj[0].getDate() + ' ' + month[dates_obj[0].getMonth()]);
            $('.js-selected-dates-finish').text(dates_obj[1].getDate() + ' ' + month[dates_obj[1].getMonth()]);
        },
        prev: '',
        next: '',
        monthChange: function(count) {
            if (count === 1) {
                $('.js-month-selector-change-trigger').parent().filter('.-state-selected').removeClass('-state-selected').next().addClass('-state-selected');
                months_to_skip_old++;
            } else if (-1) {
                $('.js-month-selector-change-trigger').parent().filter('.-state-selected').removeClass('-state-selected').prev().addClass('-state-selected');
                months_to_skip_old--;
            }
        }
    }).pickmeup('update');

    $('.js-month-selector-change-trigger').on('click', function () {
        var months_to_skip_data = $(this).data('month-to-skip'),
            months_to_skip,
            pmu_instance = $pmu.data('pickmeupOptions');

        if (typeof months_to_skip_data !== 'undefined') {
            months_to_skip = months_to_skip_data - months_to_skip_old;

            pmu_instance.current.addMonths(months_to_skip);
            pmu_instance.binded.fill();

            months_to_skip_old = months_to_skip_data;

            $(this).parent().addClass('-state-selected').siblings().removeClass('-state-selected');
        }
    });

    $('.js-calendar-reset').on('click', function () {
        $pmu.pickmeup('set_date', current_date);
        $('.js-month-selector-change-trigger').parent().removeClass('-state-selected').siblings().eq(0).addClass('-state-selected');
        $('.js-selected-dates-start, .js-selected-dates-finish').text(current_date.getDate() + ' ' + month[current_date.getMonth()]);
    });

    $('.js-calendar-next').on('click', function () {
       var selected_dates = $pmu.pickmeup('get_date', false);
       alert('Selected dates: ' + selected_dates[0].getDate() + ' ' + month[selected_dates[0].getMonth()] + ' — ' + selected_dates[1].getDate() + ' ' + month[selected_dates[1].getMonth()]);
    });
});
